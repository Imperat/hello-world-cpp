#include <iostream>
#include <cstdlib>

int main() {
  std::cout << "Hello, World!!!" << std::endl;
  if(const char* env_p = std::getenv("ENV"))
          std::cout << "Your ENV is: " << env_p << '\n';

  return 0;
}
